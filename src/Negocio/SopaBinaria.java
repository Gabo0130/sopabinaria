/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//@author Jhony Quintero y Jeison Sepulveda
package Negocio;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Jhony Quintero y Jeison Sepulveda
 */
public class SopaBinaria {
    
    int mySopaBinaria[][];
    int binario[];
    int binaryInv[]; 
    private int canFilas;
    private int cantCol;
    private int de;
    
   
    public SopaBinaria() {
        /**
     *llena datos por defecto para la clase prueba
     */
        try {
            SopaBinaria("src/datos/sopaBinaria3.xls");
            binario(7);
            getCuantasVeces_Horizontal();
        } catch (IOException ex) {
            Logger.getLogger(SopaBinaria.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    /**
     *obtiene el archivo excel con extension .xls 
     * llena una matriz con los datos almacenados en el archivo 
     */
     public void SopaBinaria(String rutaArchivoExcel) throws FileNotFoundException, IOException {
         HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(rutaArchivoExcel));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        this.canFilas = hoja.getLastRowNum()+1;
        this.mySopaBinaria=new int[canFilas][];
      
        for (int i = 0; i < canFilas; i++) {
            HSSFRow filas = hoja.getRow(i);
            this.cantCol=filas.getLastCellNum();
            this.mySopaBinaria[i]=new int[this.cantCol];
            
            
        for(int j=0;j<cantCol;j++)    
        {int valor=Integer.parseInt(filas.getCell(j).getStringCellValue());
            this.mySopaBinaria[i][j]=valor;
        }
     
       }
        
    }
/**
     *Obtiene el nunmero decimal y lo convierte a binario 
     * Guarda el binario obtenido en una arreglo 
     */
    public void binario(int decimal) {
        this.de=decimal;
     int binaryInv2[] = new int[40]; 
     int index = 0;    
     while(decimal > 0){    
      binaryInv2[index++] = decimal%2;
       
       decimal = decimal/2;    
     } 
     this.binario= new int[index];
              for(int i = index-1;i >= 0;){ 
         for(int j=0;j<binario.length;j++){
         this.binario[j]=binaryInv2[i];   
         i--;
                 
     }    
        }
      
     this.binaryInv=new int[index];
        for(int i = index-1;i >= 0;)
            { 
             for(int j=0;j<binaryInv.length;j++)
                {
                 this.binaryInv[i]=binaryInv2[i];   
                   i--;
                } 
            }
    
 }
    
    //@return retorna las veces que encuentra el numero binario en horizontal
    public int getCuantasVeces_Horizontal()
    {
        int horizontal=0;
        //recorre la matriz y lo compara con el numero binario 
        for (int i = 0; i <this.mySopaBinaria.length;i++) {
             
             for(int j=0;j<this.mySopaBinaria[i].length;j++)    
              { 
                  //aux sirve para no modificar a j mientras valida si esta el binario desde j 
                int aux=j;
                for(int k=0;k<this.binario.length;){ 
                    //condicional para que no busque posiciones en los arreglos que no existen 
                   if(aux<this.mySopaBinaria[i].length&&k<this.binario.length){
                       //cuando k este en la ultima posicion del arreglo y la posicion de la matriz sean iguales, el numero binario sera encontrado 
                       if(k==this.binario.length-1&&(this.mySopaBinaria[i][aux]==this.binario[k])){
                         horizontal++;
                         k=this.binario.length;
               
                         }
                          //busca el binario desde la posicion j hasta la posicion final del binario 
                         else if(this.mySopaBinaria[i][aux]==this.binario[k]){
                          k++;
                          aux++;
                          }else{
                          k=this.binario.length;
                         }
                    }else{k=this.binario.length;}
                }
            }
        }
          
        return horizontal;
    }
    
    //@return retorna las veces que encuentra el numero binario en vertical 
  public int getCuantasVeces_Vertical()
    {int vertical=0;
    int filas=this.canFilas;
    int columnas=this.cantCol;
    /**
     * recorre la matriz y lo compara con el numero binario, ahora de forma vertical
     * es proceso es muy parecido al de buscarlo horizontalmente 
     * solo que cambian de posiciones la variable para buscarlos de manera vertical 
     */
        for (int i = 0; i <columnas;i++) {
             
         for(int j=0;j<filas;j++)    
        { 
            int aux=j;
           for(int k=0;k<this.binario.length;){ 
               if(aux<filas&&k<this.binario.length){
               if(k==this.binario.length-1&&(this.mySopaBinaria[aux][i]==this.binario[k])){
                vertical++;
                k=this.binario.length;
               
                 }else if(this.mySopaBinaria[aux][i]==this.binario[k]){
                    k++;
                   aux++;
           
                    }else{
                      k=this.binario.length;
                    }
                }else{k=this.binario.length;}
            }
        }
     
       }
          
        return vertical;
    }
  
    
   /**
    * @return retorna las veces que encuentra el numero binario en diagonal
    * Tiene en cuenta si el binario es capicúa 
    
     */ 
    public int getCuantasVeces_Diagonal()
    {   int diagonalHaciaBajo=0;
        int diagonalHaciaArriba=0;
        int filas=this.canFilas;
        int columnas=this.cantCol;
        int tamBin=this.binario.length;
        //halla el numero en diagonal hacia abajo 
        for (int i = 0; i <filas;i++) {
             
           for(int j=0;j<columnas;j++)    
          { //aux sirve para no modificar a j ni a i mientras valida si esta el binario desde j e i 
            int aux1=i;
            int aux2=j;
            //condicional para que no busque posiciones en los arreglos que no existen 
              if(aux1+tamBin-1<filas&&aux2+tamBin-1<columnas){
                 for(int k=0;k<tamBin;k++){
                   if(k==this.binario.length-1&&(this.mySopaBinaria[aux1][aux2]==this.binario[k])){
                        diagonalHaciaBajo++;
                        k=this.binario.length;
               
                     }else if(this.mySopaBinaria[aux1][aux2]==this.binario[k]){
                      aux1++;
                      aux2++;
                     }else{ 
                      k=tamBin;
                          }
                     
                 }
              }
          
          }
        }
      /**halla el numero en diagonal hacia arrib
       * si es capicúa, no lo busca porque es el mismo encontrado del proceso anterior
       * se me ocurrio invertir el binario y buscarlo de la misma manera del proceso anterior 
       * 
       */
      if(EsCapicua()==false){
          
         for (int i = 0; i <filas;i++) {
             
           for(int j=0;j<columnas;j++)    
          { int aux1=i;
            int aux2=j;
              if(aux1+tamBin-1<filas&&aux2+tamBin-1<columnas){
                 for(int k=0;k<tamBin;k++){
                   if(k==this.binaryInv.length-1&&(this.mySopaBinaria[aux1][aux2]==this.binaryInv[k])){
                        diagonalHaciaBajo++;
                        k=this.binaryInv.length;
               
                     }else if(this.mySopaBinaria[aux1][aux2]==this.binaryInv[k]){
                      aux1++;
                      aux2++;
                     }else{ 
                      k=tamBin;
                          }
                     
                 }
              }
          
          }
        }
      }
        return diagonalHaciaArriba+diagonalHaciaBajo;
    }
    //@return retorna la matriz en string para imprimir 
    public String toString(){
        String msg = "";
        for(int i=0; i<this.mySopaBinaria.length; i++){
            for(int j=0; j<this.mySopaBinaria[i].length; j++){
                if(this.mySopaBinaria[i][j]==1)
                    msg+= 1+"          \t";
                else
                    msg+= 0+"          \t";
            }
            msg+="\n"+"\n";
        }
        return msg;
    }
     //@return retorna el binario para poderlo imprimir 
     public String toStringB(){
        String msg = "";
        for(int i=0; i<this.binario.length; i++){
            if(this.binario[i]==1)
                    msg+= 1+"";
                else
                    msg+= 0+"";
        }
        return msg;
    }
    //@return obtiene el numero binario 
   public int[] getBinario() {
        return binario;
    }
   //@return obtiene la matriz
    public int[][] getSopa() {
        return mySopaBinaria;
    }
    //@return Metodo para saber si el numero es capicúa 
    public Boolean EsCapicua(){
    
        Boolean capicua=true;
        for(int i=0;i<this.binario.length;i++){
        for(int j=this.binaryInv.length-1;j>=0;j--)
            if(this.binario[i]!=this.binaryInv[j])
             capicua=false;
        }
        
        
        return capicua;
    }
    //@return obtiene el total de incidencias del numero binario en la matriz 
    public int getTotal(){
        
    return getCuantasVeces_Horizontal()+getCuantasVeces_Vertical()+getCuantasVeces_Diagonal();
    }
    // metodo para crear y abrir el archivo pdf
    public void crearInforme_PDF() throws Exception
    {Document documento = new Document();
    //2. Crear el archivo de almacenamiento--> PDF
     FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/SopaBinaria.pdf");
     //3. Asignar la estructura del pdf al archivo físico:
     PdfWriter.getInstance(documento,ficheroPdf);
     documento.open();
     Paragraph parrafo1 = new Paragraph();
     parrafo1.setAlignment(1);
     parrafo1.add("SOPA BINARIA"+"\n"+"\n");
     Paragraph parrafo2 = new Paragraph();
     parrafo2.setAlignment(1);
     parrafo2.add("El numero "+this.de+" que en binario es "+toStringB()+" se encontro "+getTotal()+ " veces en total"+"\n"+"\n");
     Paragraph parrafo3 = new Paragraph();
     parrafo3.setAlignment(1);
     parrafo3.add(this.toString());
     documento.add(parrafo1);
     documento.add(parrafo2);
     documento.add(parrafo3);
     documento.close();
     
     File path = new File("src/Datos/SopaBinaria.pdf");
     Desktop.getDesktop().open(path);
    }
}
